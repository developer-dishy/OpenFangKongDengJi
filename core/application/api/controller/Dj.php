<?php


namespace app\api\controller;


use app\api\model\DjInfo;
use app\api\model\FkInfo;
use think\facade\Request;

class Dj
{
    function fetch_housing_name()
    {
        $map['uuid'] = Request::put('fk_uuid');
        $fkModel = new FkInfo();
        $reponse['status'] = false;
        $reponse['data'] = $fkModel->where($map)->field('housing_name,company_name,uuid')->find();
        if ($reponse['data']) {
            $reponse['status'] = true;
        }
        return $reponse;
    }
    function dj_init(){
        $response['status'] = false;
        $request = Request::put();
        $DjModel = new DjInfo();
        $response['data'] = $DjModel->where($request)->order('id desc')->find();
        if ($response['data']){
            $response['status'] = true;
            return $response;
        }
    }
    function check_in()
    {

        $response['status'] = false;
        $request = Request::put();
        unset($request['id']);
        unset($request['createtime']);
        $request['ipaddr'] = Request::ip();
        $request['createtime'] = time();

        $DjModel = new DjInfo();
        //判断外出间隔
        if ('出' === $request['direction']) {
            $map['fingerid'] = $request['fingerid'];
            $map['direction'] = '出';
            //找到上一次外出时间
            $outDirection = DjInfo::where($map)->order('id desc')->field('createtime')->find();
            $timediff = timediff($request['createtime'], $outDirection['createtime']);
            if ($timediff['day'] < 2) {
                $request['commit'] .= '【此住户外出间隔不足2日】';
                $response['msg'] = '警告：您的外出间隔不足2日,上次外出时间为'.date('Y-m-d h:i:s',$outDirection['createtime']);
                $response['status'] = false;
                $response['data'] = $request;
                return $response;
            }
        }
        if ($save_status = $DjModel->save($request)) {
            $response['data'] = $save_status;
                $response['msg'] = '信息登记成功!（请出示此页面给工作人员）';
            $response['status'] = true;
        }
        return $response;
    }
    function force_check_in(){
        $response['status'] = false;
        $request = Request::put();
        unset($request['id']);
        $DjModel = new DjInfo();
        if ($save_status = $DjModel->save($request)) {
            $response['data'] = $save_status;
            $response['msg'] = '强行外出成功!（请出示此页面给工作人员）';
            $response['status'] = true;
        }
        return $response;
    }
}